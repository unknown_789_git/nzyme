<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'wordpress');

/** MySQL database password */
define('DB_PASSWORD', '8b92fe750f57e1203f715538d6a043fc61038689d2321cfb');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'd0H$$f&3IP{k9yOIiPb3 >pP0xZs1kVAQ[&TbyCid*:JQ8~EEr!U<w|A6K4Vhapk');
define('SECURE_AUTH_KEY',  ')tU|,=:)pOH{<QrtKaI`Xfy,eEsDOlgcaC<KdA^2p0>$!Yi<~k!jle2?suJkw;z`');
define('LOGGED_IN_KEY',    'nICd;U1%F)I*/7 [y^lHBfb#LGqr#e/SX&{)J:)L4;<f}n=D;vl^d$Kf_d^gB`e_');
define('NONCE_KEY',        '%+@FMKm~B=<,R@XNylp|pS+^wl2a&}{]5;UaGjFHrS{|7c]mu}fE>tGBD35%|{Ed');
define('AUTH_SALT',        ' Hz+;5nZiD0G|U(I4JUeDJK{^7F*k&v9|`,6MkIhyz:&+X-y*vP[u<;C7WzSrIq{');
define('SECURE_AUTH_SALT', 'M*,8|A){4)-HSFDi[1!0Wc}/r~PD8ZxdcSi0>m/S&w^C-xja4~e)V+4dstSYEm5u');
define('LOGGED_IN_SALT',   'eait~Eg7rWJ<wV0$]G|w3Wdvo6z}PdJ=9_-w{dwjlQq_6Abyi`C1;&]uiVo7T91k');
define('NONCE_SALT',       'jF8[OuooJ`1PDra+B3)I{9%%Kv4h!3V+HuFwx$-hAe|yH-JQ3lj5.,)6:pQdp&IF');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
